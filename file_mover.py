import os
import shutil

def get_all_files(base_dir, ret_list):
    for file in os.listdir(base_dir):
        if os.path.isdir(os.path.join(base_dir, file)):
            get_all_files(os.path.join(base_dir, file), ret_list)
        else:
            ret_list.append(os.path.join(base_dir, file))

def get_all_possible_files(base_dir):
    ret = []
    get_all_files(base_dir, ret)
    return ret

def move_every_file_from_src_to_dst(src_dir, dst_dir):
    for file in get_all_possible_files(src_dir):
        try:
            shutil.move(file, dst_dir)
        except:
            print(file + " failed to transfer")

src_dir = "C:\\Users\\EF\\Desktop\\Weapon\\"
dst_dir = "C:\\Users\\EF\\Desktop\\sprites\\"

move_every_file_from_src_to_dst(src_dir, dst_dir)