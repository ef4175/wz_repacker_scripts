import os
import shutil

def prepend_wz_id_to_all_files(base_dir):
    for file in os.listdir(base_dir):
        if os.path.isdir(os.path.join(base_dir, file)):
            prepend_wz_id_to_all_files(os.path.join(base_dir, file))
        else:
            wz_id = base_dir[-12:-4]
            if file[0:8] != wz_id:
                final_name = wz_id + "." + file
                shutil.move(os.path.join(base_dir, file),
                    os.path.join(base_dir, final_name))


src_dir = "C:\\Users\\EF\\Desktop\\Weapon\\"

prepend_wz_id_to_all_files(src_dir)